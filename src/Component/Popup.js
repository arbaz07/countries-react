import React from "react";

export default function Popup(props) {
  console.log('props :>> ', props);
  if(props.data===''){
    return
  }
  let boundry;
  let currencies;
  let native = Object.values(Object.values(props.data.name.nativeName)[0])[0]
  if(props.data.currencies===undefined){
    currencies = "Not Found"
  }else{
    currencies = Object.keys(props.data.currencies)[0]
  }let population = props.data.population
  let lang = Object.values(props.data.languages).toString()  
  let subRegion = props.data.subregion;
  let capital;
  if(props.data.capital===undefined){
    capital='Not Found'
  }else{
    capital = props.data.capital.toString();
  }
  if(props.data.borders===undefined){
    boundry='No Border Countries'
  }else{
    boundry = props.data.borders.toString();
  }
  console.log(boundry);




  return (
    <div className="popup">
      <img src={props.data.flags.svg} alt='flag' />
      <div>
        <h1>{props.data.name.common}</h1>
     
      <div className="details">
        <p>
          <strong>Native Name : </strong>{native}
        </p>
        <p>
          <strong>Top Level Domain :</strong>be
        </p>
        <p>
          <strong>Currency : </strong>{currencies}
        </p>
        <p>
          <strong>Population : </strong>{population}
        </p>
        <p>
          <strong>Region : </strong>{props.data.region}
        </p>
        <p>
          <strong>Language :</strong>{lang}
        </p>
        <p>
          <strong>Sub Region : </strong>{subRegion}
        </p>
        <p>
          <strong>Capital : </strong>{capital}
        </p>
      </div>
      <div className="closeButton">
        <strong>Border</strong>
        <div className="borderCountries">{boundry}</div>
      </div>
      <div className="bottombtn">
        <button className="btn" id='back' onClick={()=>{props.disappear()}}>clear</button>
      </div>

    </div>
    </div>
  );
}
