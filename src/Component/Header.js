import React, { Component } from 'react'

export default class Header extends Component {


  render() {
    return (
      <div className='header'>
        <div className="heading">
          <h1>Where in world</h1>
          <button>Dark Mode</button>
        </div>
    

        <div className="filter-row">
          <div >
            <i className="fa fa-search icon"></i>
            <input
              onChange={(e)=>this.props.search(e.target.value)}
              type="text"
              className="btn"
              placeholder="Search country"
            />    
          </div>
          <div className="select-options">
            <label className='label'>Filter By region</label>
            <select className="btn"id="regionId" onChange={(e)=>this.props.searchByRegion(e.target.value)} size="0">
              <option className='region' >All</option>
              <option className='region' value="Africa" >Africa</option>
              <option className='region' value="Asia" >Asia</option>
              <option className='region' value="America" >America</option>
              <option className='region' value="Europe" >Europe</option>
              <option className='region' value="Oceania" >Oceania</option>
            </select>
          </div>
        </div>
        <div><h3>Double Click to View Details</h3></div>
      </div>
    )
  }
}