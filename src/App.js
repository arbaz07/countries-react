import React, { Component } from "react";
import CountryCard from "./Component/CountryCard";
import Header from "./Component/Header";
import Loader from "./Component/Loader";
import Popup from "./Component/Popup";
import Error from "./Component/Error";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countryFound:true,
      items: [],
      popup: "",
      DataisLoaded: false,
      errorOccurred: false,
      selectedCountry: "",
      searchBy:{
        key:'',
        region:'ALL',
      },
      // loader:<span class="loader"></span>,
    };
  }
  
  findSelectedCountry = (name) => {
   
    let countryObject = {};
    this.state.items.forEach((item) => {
      if (item.name.common === name) {
        countryObject = item;
      }
    });
    this.setState({
      selectedCountry: countryObject,
      popup: (
        <Popup
          data={this.state.selectedCountry}
          disappear={this.popupDisappear}
        />
      ),
    });
  };
  popupDisappear = () => {
    console.log(" popup disappear:>> ");
    this.setState({
      popup: "",
      selectedCountry:"",
    });
  };

  componentDidMount = () => {
    fetch("https://restcountries.com/v3.1/all")
      .then((res) => res.json())
      .then((json) => {
        this.setState({
          items: json,
          DataisLoaded: true,
        });
      })
      .catch(() => {
        this.setState({
          errorOccurred: true,
        });
      });
  };

  filerByCountry = (name) => {
    name = name.toUpperCase();
     this.setState({
        searchBy:{
          key:name,
          region: this.state.searchBy.region,
        }
      
    })
  };



  filerByRegion = (regionName) => {
    regionName = regionName.toUpperCase();
    console.log("name :>> ", regionName);
    this.setState({
      searchBy:{
        key: this.state.searchBy.key,
        region:regionName,
      }
    })
  }
   
  makeCountryCard = (country,index)=>{
    return (
      <CountryCard
        key={index}
        imgLink={country.flags.svg}
        countryName={country.name.common}
        countryPopulation={country.population}
        countryRegion={country.region}
        countryCapital={country.capital}
        findSelectedCountry={this.findSelectedCountry}
      />
    );  
   }

  render() {
    return (
      <div>
        {/* {this.state.errorOccurred? <Error/>: */}

        {this.state.DataisLoaded ? (
          <div className="container">
            <Header
              className="header"
              search={this.filerByCountry}
              searchByRegion={this.filerByRegion}
            />
            <div className="popupDiv">{this.state.popup}</div>

            <div className="all-countries">
              {
              
              (this.state.items.length===0)?
              <div className='message'><h1 id='messageText'>Country Not Found !!</h1></div>
              :
              
              this.state.items.map((country, index) => {
                if(this.state.searchBy.key==='' && this.state.searchBy.region==='ALL'){
                  return this.makeCountryCard(country,index)
                }else{
                  if(this.state.searchBy.key!==''){
                    if(this.state.searchBy.region==='ALL'){
                      if(country.name.common.toUpperCase().includes(this.state.searchBy.key)){
                        return this.makeCountryCard(country,index)
                      }
                    }else{
                      if(country.name.common.toUpperCase().includes(this.state.searchBy.key) && country.region.toUpperCase()===this.state.searchBy.region){
                        return this.makeCountryCard(country,index)
                      }
                    }
                  }else{
                    if(this.state.searchBy.region===country.region.toUpperCase()){
                      return this.makeCountryCard(country,index)
                    }

                  }
                }
              })}
            </div>
          </div>
        ) : (
          <div>{this.state.errorOccurred ? <Error /> : <Loader />}</div>
        )}
      </div>
    );
  }
}

export default App;
